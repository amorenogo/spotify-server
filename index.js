require('dotenv').config();
const cors = require('cors');
const express = require("express");
const SpotifyToken = require("./SpotifyToken");

const spotifyToken = new SpotifyToken();
const server = express();
const router = express.Router();
server.use(cors())

router.get('/token', async (req, res) => {
    if(spotifyToken.isTokenExpired()) {
        await spotifyToken.getSpotifyToken();
        res.status(200).json({
            data: spotifyToken.token
        })
    } else {
        res.status(200).json({
            data: spotifyToken.token
        })
    }
})
server.use(router);

const PORT  = process.env.PORT || 3000;
server.listen(PORT, () => console.log(`listening in localhost:${ PORT }`));