const axios = require('axios');
const qs = require('querystring');

class SpotifyToken {
    constructor(){
        this.token = '';
        this.expiration = '';
    }

    isTokenExpired() {
        if(!this.expiration) {
            return true;
        } 
        const now = new Date();
        return now >= this.expiration;
    } 

    getSpotifyToken() {
        const body = {
            "grant_type": process.env.GRANT_TYPE,
            "client_id": process.env.CLIENT_ID,
            "client_secret": process.env.CLIENT_SECRET
        }

        const config = {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
        }

        return axios.post("https://accounts.spotify.com/api/token", qs.stringify(body), config)
            .then((res) => {
                this.token = res.data.access_token;

                const now = new Date();
                now.setHours(now.getHours() + res.data.expires_in/3600)
                this.expiration =  now;
            })
            .catch((err) => console.log(err))
    }

}

module.exports = SpotifyToken;